﻿using System;
using System.IO;
using Microsoft.Win32;

namespace MoneyManagerWPF.Data
{
    class RegistryHandler
    {
        public static string AppKey = @"Software\MoneyManager";

        public static void CreateKey()
        {
            try
            {
                var handler = new DataHandler();
                handler.SetupDefaultFile();

                var key = Registry.CurrentUser.OpenSubKey("Software", true);
                key.CreateSubKey("MoneyManager");
                key = key.OpenSubKey("MoneyManager", true);
                key.CreateSubKey("Settings");
                key = key.OpenSubKey("Settings", true);
                key.SetValue("LastOpenedPath", new FileInfo(handler.DefaultPath).FullName);
                key.Close();
            }
            catch
            {
                throw new Exception("An error occured while creating a new key");
            }
        }

        public static void EditPathKey(string openedPath)
        {
            try
            {
                var key = Registry.CurrentUser.OpenSubKey(AppKey + "\\Settings", true);
                key.SetValue("LastOpenedPath", openedPath);
                key.Close();
            }
            catch
            {
                throw new Exception("An error occurred while editing key");
            }
        }

        /// <summary>
        /// Reads the value set in the windows registry to find the last opened filepath
        /// </summary>
        /// <returns></returns>
        public static string ReadPathKey()
        {
            var key = Registry.CurrentUser.OpenSubKey(AppKey + "\\Settings", true);
            var value = string.Empty;

            try
            {
                value = key.GetValue("LastOpenedPath") as string;
            }
            catch
            {
                throw new Exception("Error retrieving settings value");
            }

            if (string.IsNullOrWhiteSpace(value))
            {
                throw new Exception("File path was not able to be read");
            }

            key.Close();
            return value;
        }
    }
}
