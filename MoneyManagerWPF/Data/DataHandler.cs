﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using MoneyManagerWPF.Exceptions;
using MoneyManagerWPF.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MoneyManagerWPF.Data
{
    class DataHandler
    {
        public string DefaultPath { get; } = @"new.budget";

        public void WriteData(List<BudgetItem> items, string filepath)
        {
            if (string.IsNullOrEmpty(filepath))
            {
                filepath = DefaultPath;    
            }

            var stream = new FileStream(filepath, FileMode.OpenOrCreate, FileAccess.Write);
            var formatter = new BinaryFormatter();
            formatter.Serialize(stream, items);
            stream.Close();
        }

        public List<BudgetItem> ReadData(string filepath)
        {
            var stream = new FileStream(filepath, FileMode.Open, FileAccess.Read);
            var formatter = new BinaryFormatter();

            if (stream.Length == 0)
                throw new FileContainsNoDataException("File does not contain any data");

            var list = (List<BudgetItem>)formatter.Deserialize(stream);
            stream.Close();

            return list;
        }

        public void SetupDefaultFile()
        {
            File.Create(DefaultPath).Close();

            WriteData(new List<BudgetItem>(), DefaultPath);
        }
    }

    
}
