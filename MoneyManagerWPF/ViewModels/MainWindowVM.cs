﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using MoneyManagerWPF.Models;

namespace MoneyManagerWPF.ViewModels
{
    class MainWindowVM : ViewModelBase
    {
        public ExpensesVM ExpensesVM => (ExpensesVM) CurrentViewModel;
        private ViewModelBase _currentViewModel { get; set; }
        public ViewModelBase CurrentViewModel
        {
            get => _currentViewModel;
            set
            {
                _currentViewModel = value;
                RaisePropertyChanged();
            }
        }

        public MainWindowVM()
        {
            CurrentViewModel = new ExpensesVM();
            Messenger.Default.Register<ViewChangeMessage>
            (
                this, ReceiveViewMessage
            );
        }

        private void ReceiveViewMessage(ViewChangeMessage msg)
        {
            if (msg.ViewName.Equals("Expenses"))
            {
                CurrentViewModel = new ExpensesVM();
                Messenger.Default.Send(msg.View);
            }
        }
    }
}
