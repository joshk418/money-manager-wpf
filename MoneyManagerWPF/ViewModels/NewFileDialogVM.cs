﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Ookii.Dialogs.Wpf;

namespace MoneyManagerWPF.ViewModels
{
    public class NewFileDialogVM : ViewModelBase
    {
        #region Commands
        public RelayCommand SelectFileLocationCommand { get; set; }
        public RelayCommand OkDialogCommand { get; set; }
        public RelayCommand CancelDialogCommand { get; set; }
        #endregion

        #region Private Properties
        private string _filepath { get; set; }
        #endregion

        #region Properties
        public Action CloseAction { get; set; }
        public bool DialogResult { get; set; }
        public string FileName { get; set; }

        public string FilePath
        {
            get => _filepath;
            set
            {
                _filepath = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region Constructor
        public NewFileDialogVM(Action closeAction)
        {
            // Set up commands
            SelectFileLocationCommand = new RelayCommand(SelectFileLocation);
            OkDialogCommand = new RelayCommand(OkDialog);
            CancelDialogCommand = new RelayCommand(CancelDialog);

            CloseAction = closeAction;
        }
        #endregion

        #region Methods
        private void SelectFileLocation()
        {
            var dialog = new VistaFolderBrowserDialog();
            var dialogResult = dialog.ShowDialog();

            if (dialogResult == false)
            {
                return;
            }

            FilePath = dialog.SelectedPath;
        }

        private void OkDialog()
        {
            if (ValidateEntry() == false)
            {
                return;
            }

            if (FileName.Contains('.'))
            {
                FileName = FileName.Split('.')[0];
            }

            DialogResult = true;
            CloseAction();
        }

        private void CancelDialog()
        {
            DialogResult = false;
            CloseAction();
        }

        private bool ValidateEntry()
        {
            if (string.IsNullOrWhiteSpace(FileName))
            {
                MessageBox.Show("Please select a name", "Invalid Entry", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            if (string.IsNullOrWhiteSpace(FilePath))
            {
                MessageBox.Show("Please select a folder", "Invalid Entry", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            return true;
        }
        #endregion
    }
}
