﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using GalaSoft.MvvmLight;
using Microsoft.Win32;
using System.IO;
using GalaSoft.MvvmLight.Command;
using MoneyManagerWPF.Data;
using MoneyManagerWPF.Models;
using MoneyManagerWPF.Views;

namespace MoneyManagerWPF.ViewModels
{
    class ExpensesVM : ViewModelBase
    {
        #region Variables
        private DataHandler _handler;
        #endregion  

        #region RelayCommands
        public RelayCommand SubmitDataCommand { get; set; }
        public RelayCommand LoadDataCommand { get; set; }
        public RelayCommand ClearDataCommand { get; set; }
        public RelayCommand CreateNewFileCommand { get; set; }
        public RelayCommand DeleteBudgetItemCommand { get; set; }
        public RelayCommand ExitApplicationCommand { get; set; }
        public RelayCommand OpenHelpCommand { get; set; }
        #endregion

        #region Private Properties
        private List<BudgetItem> _budgetItems { get; set; }
        private string _totalBalanceDisplay { get; set; }
        private string _titleInput { get; set; }
        private string _amountInput { get; set; }
        private string _categoryInput { get; set; }
        private string _windowTitle { get; set; }
        private Brush _balanceColor { get; set; }
        #endregion

        #region Properties
        public string OpenedFilePath { get; set; }

        public BudgetItem SelectedBudgetItem { get; set; }

        public ObservableCollection<BudgetItem> BudgetItems { get; set; }

        public string WindowTitle
        {
            get => _windowTitle;
            set
            {
                _windowTitle = value;
                RaisePropertyChanged();
            }
        }

        public Brush BalanceColor
        {
            get =>_balanceColor;
            set
            {
                _balanceColor = value;
                RaisePropertyChanged();
            }
        }

        public string TotalBalanceDisplay
        {
            get => _totalBalanceDisplay;
            set
            {
                _totalBalanceDisplay = value;
                RaisePropertyChanged();
            }
        }
        
        //Inputs
        public string TitleInput
        {
            get => _titleInput;
            set
            {
                if (!string.Equals(_titleInput, value))
                {
                    _titleInput = value;
                    RaisePropertyChanged();
                }
            }
        }

        public string AmountInput
        {
            get => _amountInput;
            set
            {
                if (!string.Equals(_amountInput, value))
                {
                    _amountInput = value;
                    this.RaisePropertyChanged();
                }
            }
        }

        public string CategoryInput
        {
            get => _categoryInput;
            set
            {
                if (!string.Equals(_categoryInput, value))
                {
                    _categoryInput = value;
                    RaisePropertyChanged();
                }
            }
        }
        #endregion

        #region Constructor
        public ExpensesVM()
        {
            GenerateNewUserSettings();

            _handler = new DataHandler();
            BudgetItems = new ObservableCollection<BudgetItem>();

            try
            {
                OpenedFilePath = RegistryHandler.ReadPathKey();
            }
            catch (Exception ex)
            {
                var result = MessageBox.Show($"{ex.Message}. Would you like to open a file now?", "No File Found",
                    MessageBoxButton.YesNo, MessageBoxImage.Warning);

                if (result == MessageBoxResult.No)
                {
                    Application.Current.Shutdown();
                }

                LoadData();
            }
            

            // Commands
            SubmitDataCommand = new RelayCommand(SubmitData);
            LoadDataCommand = new RelayCommand(LoadData);
            ClearDataCommand = new RelayCommand(ClearData);
            CreateNewFileCommand = new RelayCommand(CreateNewFile);
            DeleteBudgetItemCommand = new RelayCommand(DeleteBudgetItem);
            ExitApplicationCommand = new RelayCommand(ExitApplication);
            OpenHelpCommand = new RelayCommand(OpenHelp);

            //Populate data and observable lists
            _budgetItems = _handler.ReadData(OpenedFilePath);

            _balanceColor = Brushes.Black;

            //Set inputs to avoid null object reference
            TitleInput = string.Empty;
            AmountInput = string.Empty;
            CategoryInput = string.Empty;

            UpdateUI();
        }

        #endregion

        #region Methods
        private void SubmitData()
        {
            if (ValidateUserEntry() == false)
            {
                return;
            }

            var item = new BudgetItem
            {
                Title = TitleInput,
                Amount = double.Parse(AmountInput),
                Type = CategoryInput.Equals("Charge") ? BudgetItemType.Charge : BudgetItemType.Payment
            };

            _budgetItems.Add(item);

            ClearInputs();
            UpdateUI();
            SaveCurrentFile();
        }

        private void LoadData()
        {
            var dialog = new OpenFileDialog();
            var result = dialog.ShowDialog();

            if (result == false)
            {
                return;
            }

            var path = new FileInfo(dialog.FileName).FullName;

            try
            {
                _budgetItems = _handler.ReadData(path);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            OpenedFilePath = path;


            UpdateUI();

            SaveRegistryOpenedFilePath();
        }

        private void ClearData()
        {
            var result = MessageBox.Show("Are you sure you want to delete all data in this file?", "Are you sure",
                MessageBoxButton.YesNo, MessageBoxImage.Exclamation);

            if (result == MessageBoxResult.No)
            {
                return;
            }

            _budgetItems.Clear();

            UpdateUI();
            SaveCurrentFile();
        }

        private void CreateNewFile()
        {
            var dialog = new NewFileDialog();
            dialog.ShowDialog();

            if (dialog.ViewModel.DialogResult == false)
            {
                return;
            }

            var fullPath = dialog.ViewModel.FilePath + "\\" + dialog.ViewModel.FileName + ".budget";
            _handler.WriteData(new List<BudgetItem>(), fullPath);
            _budgetItems = _handler.ReadData(fullPath);
            OpenedFilePath = fullPath;

            UpdateUI();

            SaveRegistryOpenedFilePath();

            MessageBox.Show("File created", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void DeleteBudgetItem()
        {
            BudgetItems.Remove(SelectedBudgetItem);
            _budgetItems = BudgetItems.ToList();
            UpdateUI();
            SaveCurrentFile();
        }

        private void ExitApplication()
        {
            Application.Current.Shutdown();
        }

        private void OpenHelp()
        {
            var help = new HelpWindow();
            help.ShowDialog();
        }

        private void UpdateUI()
        {
            BudgetItems.Clear();

            _budgetItems.ForEach(x => BudgetItems.Add(x));

            TotalBalanceDisplay = $"${CalculateBalance():N2}";

            WindowTitle = string.IsNullOrWhiteSpace(OpenedFilePath) 
                ? "Money Manager" 
                : $"Money Manager - {OpenedFilePath}";
        }

        private void SaveCurrentFile()
        {
            _handler.WriteData(_budgetItems, OpenedFilePath);
        }

        private void ClearInputs()
        {
            TitleInput = string.Empty;
            AmountInput = string.Empty;
            CategoryInput = string.Empty;
        }

        private double CalculateBalance()
        {
            var totalExpense = 0d;
            var totalPayment = 0d;

            foreach (var item in _budgetItems)
            {
                if (item.Type == BudgetItemType.Charge)
                    totalExpense += item.Amount;
                else
                    totalPayment += item.Amount;
            }

            var result = totalPayment > totalExpense ? totalPayment - totalExpense : totalExpense - totalPayment;

            BalanceColor = GetBalanceDisplayColor(totalExpense, totalPayment);

            return result;
        }

        private Brush GetBalanceDisplayColor(double expense, double payment)
        {
            return expense > payment ? Brushes.Red : Brushes.Green;
        }

        private bool ValidateUserEntry()
        {
            if (string.IsNullOrWhiteSpace(_titleInput.Trim()) || string.IsNullOrWhiteSpace(_amountInput.Trim()))
            {
                MessageBox.Show("Please fill both fields.", "Invalid Input", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            if (string.IsNullOrWhiteSpace(_categoryInput))
            {
                MessageBox.Show("Please select a category.", "Invalid Input", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            if (!double.TryParse(_amountInput, out _))
            {
                MessageBox.Show("Please enter a valid amount.", "Invalid Input", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            return true;
        }

        private void GenerateNewUserSettings()
        {
            var defaultPath = new DataHandler().DefaultPath;
            var keyPath = RegistryHandler.AppKey;
            RegistryKey key;

            var settingsNeeded = false;

            try
            {
                if ((key = Registry.CurrentUser.OpenSubKey(keyPath + "\\Settings", true)) == null)
                {
                    RegistryHandler.CreateKey();
                    settingsNeeded = true;
                }

                if (!File.Exists(defaultPath))
                {
                    File.Create(defaultPath).Close();
                    settingsNeeded = true;
                }

                if (settingsNeeded)
                {
                    MessageBox.Show("New user settings generated", "First Time Setup",
                        MessageBoxButton.OK, MessageBoxImage.Information);
                }
                
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "New User Settings Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            
        }

        private void SaveRegistryOpenedFilePath()
        {
            try
            {
                RegistryHandler.EditPathKey(OpenedFilePath);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Unable to Save Location", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        
        #endregion
    }
}
