﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyManagerWPF.Models
{
    [Serializable]
    public class BudgetItem
    {
        public string Title { get; set; }
        public double Amount { get; set; }
        public DateTime Date { get; private set; }
        public BudgetItemType Type { get; set; }

        public BudgetItem()
        {
            Date = DateTime.Now;
        }

        public BudgetItem(string title, double amount, BudgetItemType type)
        {
            Date = DateTime.Now;

            Title = title;
            Amount = amount;
            Type = type;
        }
    }

    

    public enum BudgetItemType
    {
        Charge,
        Payment
    }
}
