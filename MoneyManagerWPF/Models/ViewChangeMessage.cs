﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;

namespace MoneyManagerWPF.Models
{
    class ViewChangeMessage
    {
        public ViewModelBase View { get; set; }
        public string ViewName { get; set; }
    }
}
