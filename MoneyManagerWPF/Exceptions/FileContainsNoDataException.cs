﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyManagerWPF.Exceptions
{
    class FileContainsNoDataException : Exception
    {
        public FileContainsNoDataException(string message)
            : base(message)
        {  }
    }
}
